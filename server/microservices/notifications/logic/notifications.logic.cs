using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Shared.Helpers;
using Shared.Data;
using Notifications.Dal;

namespace Notifications.Logic {

public interface INotificationsLogic {
    Task<Notification> FindOne(Guid id);
    Task<List<Notification>> Find();
}

public class NotificationsLogic : INotificationsLogic {

    ILogger Logger { get; } =
    AppLoggerFactory.CreateLogger<NotificationsLogic>();
    INotificationsDal _notificationsDal;

    public NotificationsLogic(INotificationsDal notificationsDal) {
        _notificationsDal = notificationsDal;
        Logger.LogInformation("Initializing NotificationsLogic.");
    }

    public async Task<Notification> FindOne(Guid id) {
        var notification = await _notificationsDal.FindOne(id);
        return notification;
    }

    public async Task<List<Notification>> Find() {
        var notifications = await _notificationsDal.Find();
        return notifications;
    }
}

}