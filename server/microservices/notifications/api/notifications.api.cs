﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Notifications.Logic;
using Shared.Data;

namespace Notifications.Controllers
{
    [Route("api/[controller]")]
    public class NotificationsController : Controller
    {
        INotificationsLogic _notificationsLogic;
        public NotificationsController(INotificationsLogic notificationsLogic) {
            _notificationsLogic = notificationsLogic;
        }

       //GET api/notifications/{id}
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(Notification), 200)]
        public async Task<IActionResult> FindOne(Guid id)
        {
            if(id == null || id == Guid.Empty) {
                return BadRequest();
            }
            var notification = await _notificationsLogic.FindOne(id);
            return new OkObjectResult(notification);
        }

        // GET api/notifications/
        [HttpGet]
        [ProducesResponseType(typeof(List<Notification>), 200)]
        public async Task<IActionResult> Find() {
            var notifications = await _notificationsLogic.Find();
            return new OkObjectResult(notifications);
        }
    }
}
