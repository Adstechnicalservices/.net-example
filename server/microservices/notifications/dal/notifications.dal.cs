using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using System;
using Shared.Helpers;
using Shared.Data;

namespace Notifications.Dal {
public interface INotificationsDal {
    Task<Notification> FindOne(Guid id);
    Task<List<Notification>> Find();
}

public class NotificationsDal : INotificationsDal {
    ILogger Logger { get; } =
    AppLoggerFactory.CreateLogger<NotificationsDal>();
    public NotificationsDal() {
        Logger.LogInformation("Initializing ActionItemsDal.");
    }

    public Task<Notification> FindOne(Guid id) {
        try {
          return  Task.FromResult(new Notification());
        }
        catch(Exception ex) {
            return null;
        }
    }
    public Task<List<Notification>> Find() {
        try {
          return  Task.FromResult(new List<Notification>());
        }
        catch(Exception ex) {
            return null;
        }
    }
}

}