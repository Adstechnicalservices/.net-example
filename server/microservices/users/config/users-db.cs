using LiteDB;
using Shared.Data.Persisted;
using System;
using System.Collections.Generic;
using System.Linq;
using Users.Config;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;

namespace Users.Config.Database {

  public interface IUsersDb {
     User GetUser(string username);
     Guid GetDelta3UserId(string username);
     List<User> GetAllUsers();
  }

  public class UsersDb :IUsersDb {

private D3UsersSeed _d3UsersSeedData;
    public UsersDb(IOptions<D3UsersSeed> config) {
      AppStartup.AppLogger.LogInformation("In the Db.");
      _d3UsersSeedData = config.Value;
      using(var db = new LiteDatabase(@"Users.db")) {
        AppStartup.AppLogger.LogInformation("In the Using.");
        var users = db.GetCollection<User>("Users");
        if(users.Count() == 0) {
          AppStartup.AppLogger.LogInformation("IUsers count is 0." + (_d3UsersSeedData.D3Users.First()));
          foreach(var user in _d3UsersSeedData.D3Users) {
            users.Insert(new User {
              Id= Guid.NewGuid(),
              Delta3Id = Guid.Parse(user.USER_ID),
              FirstName = user.FIRST_NAME,
              MiddleName = user.MIDDLE_NAME,
              LastName = user.LAST_NAME,
              Username = user.LOGIN_NAME,
              EmployeeId = user.EMPLOYEE_ID,
              Email = user.EMAIL,
              IsActive = user.IS_ACTIVE  ,
              Agency = user.AGENCY,
              Title = user.TITLE,
              Notes = user.NOTES,
              WorkAreaUrl = user.WORK_AREA_URL,
              OtherName = user.OTHER_NAME
            });
          }
        }
      }
    }

    public List<User> GetAllUsers() {
      AppStartup.AppLogger.LogInformation("In the Db - GetAllUsers.");
        // Open database (or create if doesn't exist)
        using(var db = new LiteDatabase(@"Users.db")) {
            var usersCol = db.GetCollection<User>("Users");
            var users = usersCol.FindAll();
            if(users.Count() > 0) {
              return users.ToList();
            } else {
              return new List<User>();
            }
            
        }
    }

      public User GetUser(string username) {
          // Open database (or create if doesn't exist)
        using(var db = new LiteDatabase(@"Users.db")) {
          var fullUsername = @"DELTA\" + username.Trim();
            var usersCol = db.GetCollection<User>("Users");
            var users = usersCol.Find(x=>x.Username == fullUsername);
            if(users.Count() > 0) {
              return users.First();
            } else {
              return null;
            }
            
        }
      }

      public Guid GetDelta3UserId(string username) {
          // Open database (or create if doesn't exist)
        using(var db = new LiteDatabase(@"Users.db")) {
           var fullUsername = (@"DELTA\" + username.Trim()).ToLower();
            var usersCol = db.GetCollection<User>("Users");
            var users = usersCol.Find(x=>x.Username == fullUsername);
              if(users.Count() > 0) {
                return users.First().Delta3Id;
              }
              else {
                return Guid.Empty;
              }
        }
      }
  }

}