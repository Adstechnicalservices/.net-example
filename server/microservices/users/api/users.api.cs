﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Users.Config.Database;
using Shared.Data.Persisted;
using Microsoft.Extensions.Logging;

namespace Users.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private IUsersDb _usersDb;
        public UsersController(IUsersDb usersDb) {
            _usersDb = usersDb;
            AppStartup.AppLogger.LogInformation("Creating users controller.  Users count is now " + _usersDb.GetAllUsers().Count());
        }

        //GET api/users/
        [HttpGet]
        public List<User> GetAll() {
            List<User> users = new List<User>();
            users = _usersDb.GetAllUsers();
            return users;
        }

        // GET api/users/getDelta3userId/ba.agent
        [HttpGet("getDelta3UserId/{username}")]
        public Guid GetDelta3UserId(string username)
        {
            Guid delta3Id = _usersDb.GetDelta3UserId(username);
            return delta3Id;
        }

        // GET api/users/getUser/ba.agent
        [HttpGet("getUser/{username}")]
        public User GetUser(string username)
        {
            User user = _usersDb.GetUser(username);
            return user;
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
