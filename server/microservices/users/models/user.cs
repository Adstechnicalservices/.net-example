
using System;
namespace Users.Models {
    public class User {
        public Guid Id {get;set;}
        public Guid Delta3Id {get;set;}
        public string FirstName {get;set;}
        public string LastName {get;set;}
        public string Username {get;set;}

    }
}