using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using System;
using Shared.Helpers;
using Shared.Interfaces.RestfulService;
using Shared.Data.Persisted;
using MongoDB.Bson;

namespace ActionItems.Dal {
public interface IActionItemsDal : IRestServiceDal<Guid, ActionItem> {
    Task<List<ActionItem>> FindForSources(Guid? sourceId, List<Guid> sourceIds);
}

public class ActionItemsDal : IActionItemsDal {
    ILogger Logger { get; } =
    AppLoggerFactory.CreateLogger<ActionItemsDal>();
    private Database.MongoDbContext _db;
    public ActionItemsDal(Database.MongoDbContext db) {
        Logger.LogInformation("Initializing ActionItemsDal.");
        _db = db;
    }
    public Task<List<ActionItem>> FindForSources(Guid? sourceId, List<Guid> sourceIds) {
        try {
          return  Task.FromResult(new List<ActionItem>());
        }
        catch(Exception ex) {
            return null;
        }
    }

    public async Task<ActionItem> FindOne(Guid id) {
        return null;
    }

    public async Task<List<ActionItem>> Find() {
        return null;
    }

    public async Task<ActionItem> Create(ActionItem actionItem) {
        return null;
    }

    public async Task<ActionItem> Update(Guid id, ActionItem actionItem) {
        return null;
    }

    public async Task<bool> Delete(Guid id) {
        return true;
    }
}

}