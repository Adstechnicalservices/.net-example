using Shared.Data.Persisted;

using MongoDB.Driver;
namespace ActionItems.Database {
public class MongoDbSetup {
    public static IMongoDatabase GetDatabase(string connectionString = "") {
        
        if(connectionString != "") {
            var client = new MongoClient(connectionString);
            return client.GetDatabase("action-items-db");
        }
        else {
            var client = new MongoClient();
            return client.GetDatabase("action-items-db");
        }
    }
 
}

public class MongoDbContext {
    private IMongoDatabase db;
    public MongoCollectionBase<ActionItem> actionItems;
    public MongoDbContext() {
        db = MongoDbSetup.GetDatabase();
        CreateDbContext();
    }

    private void CreateDbContext() {
       actionItems = (MongoCollectionBase<ActionItem>)db.GetCollection<ActionItem>("action-items");
    }
}

}