using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ActionItems.Logic;
using Shared.Data.Nonpersisted.Dtos;
using Microsoft.Extensions.Logging;
using Shared.Helpers;
using Shared.Interfaces.RestfulService;
namespace ActionItems.Controllers
{
    [Route("api/[controller]")]
    public class ActionItemsController : Controller, IRestServiceApi<Guid, ActionItemDto>
    {
        ILogger Logger { get; } =
            AppLoggerFactory.CreateLogger<ActionItemsController>();
        private IActionItemsLogic _actionItemsLogic;

        public ActionItemsController(IActionItemsLogic actionItemsLogic) {
            Logger.LogInformation("Initializing ActionItemsRestController.");
            _actionItemsLogic = actionItemsLogic;
        }

        //GET api/actionitems/{id}
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ActionItemDto), 200)]
        public async Task<IActionResult> FindOne(Guid id)
        {
            if(id == null || id == Guid.Empty) {
                return BadRequest();
            }
            var actionItem = await _actionItemsLogic.FindOne(id);
            return new OkObjectResult(actionItem);
        }

         //GET api/actionitems
        [HttpGet]
        [ProducesResponseType(typeof(List<ActionItemDto>), 200)]
        public async Task<IActionResult> Find()
        {
            var actionItems = await _actionItemsLogic.Find();
            return new OkObjectResult(new List<ActionItemDto>());
        }

        [HttpPost]
        [ProducesResponseType(typeof(ActionItemDto), 200)]
        public async Task<IActionResult> Create([FromBody] ActionItemDto actionItem) {
            if(actionItem == null) {
                return BadRequest();
            }

            var created = await _actionItemsLogic.Create(actionItem);
            return new OkObjectResult(created);
        }

        [HttpPost]
        [Route("{id}")]
        [ProducesResponseType(typeof(ActionItemDto), 200)]
        public async Task<IActionResult> Update(Guid id, [FromBody] ActionItemDto actionItem) {
            if(id == null || id == Guid.Empty || actionItem == null) {
                return BadRequest();
            }

            var updated = await _actionItemsLogic.Update(id, actionItem);
            return new OkObjectResult(updated);
        }

        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(typeof(void), 200)]
        public async Task<IActionResult> Delete(Guid id) {
            if (id == null || id == Guid.Empty) {
                return BadRequest();
            }
            if(await _actionItemsLogic.Delete(id)) {
                return Ok();
            }
            else {
                return BadRequest();
            }
        }

                //GET api/actionitems/getforsource/{sourceId}
        [HttpGet]
        [Route("GetForSource/{sourceId}")]
        public async Task<IActionResult> GetForSource(Guid sourceId)
        {
            if(sourceId == null || sourceId == Guid.Empty) {
                return BadRequest();
            }
            var actionItems = await _actionItemsLogic.GetForSource(sourceId);
            return new OkObjectResult(actionItems);
        }

        // POST api/actionitems/getforsources
        //Body: Array Guids
        ////This is ////
        [HttpPost]
        [Route("GetForSources")]
        [ProducesResponseType(typeof(List<ActionItemDto>), 200)]
        public async Task<IActionResult> GetForSources([FromBody] Guid[] sourceIds) {
            if(sourceIds == null || sourceIds.Count() == 0 || sourceIds.Any(i=> i == Guid.Empty)) {
                return BadRequest();
            }
            var actionItems = await _actionItemsLogic.GetForSources(sourceIds.ToList());
            return new OkObjectResult(actionItems);
        }

    }
}
