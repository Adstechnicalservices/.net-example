using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Shared.Helpers;
using Shared.Interfaces.RestfulService;
using Shared.Data.Nonpersisted.Dtos;
using Shared.Data.Persisted;
using ActionItems.Dal;
namespace ActionItems.Logic {

public interface IActionItemsLogic : IRestServiceLogic<Guid, ActionItemDto> {
    Task<List<ActionItem>> GetForSource(Guid sourceId);
    Task<List<ActionItem>> GetForSources(List<Guid> sourceIds);
}

public class ActionItemsLogic : IActionItemsLogic {

    ILogger Logger { get; } =
    AppLoggerFactory.CreateLogger<ActionItemsLogic>();
    IActionItemsDal _actionItemsDal;

    public ActionItemsLogic(IActionItemsDal actionItemsDal) {
        _actionItemsDal = actionItemsDal;
        Logger.LogInformation("Initializing ActionItemsLogic.");
    }

    public async Task<ActionItemDto> FindOne(Guid id) {
        return null;
    }

    public async Task<List<ActionItemDto>> Find() {
        return null;
    }

    public async Task<ActionItemDto> Create(ActionItemDto actionItem) {
        return null;
    }

    public async Task<ActionItemDto> Update(Guid id, ActionItemDto actionItem) {
        return null;
    }

    public async Task<bool> Delete(Guid id) {
        return true;
    }

    public async Task<List<ActionItem>> GetForSource(Guid sourceId) {
        var actionItems = await _actionItemsDal.FindForSources(sourceId, null);
        return actionItems;
    }

    public async Task<List<ActionItem>> GetForSources(List<Guid> sourceIds) {
        var actionItems = await _actionItemsDal.FindForSources(null, sourceIds);
        return actionItems;
    }
}

}