using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.Utility.Authentication;
namespace Gateway.Controllers
{
   [Route("api/[controller]")]
    public class AuthenticationController : Controller
    {
        private TokenHelper _tokenHelper;

        public AuthenticationController(TokenHelper tokenHelper) {
            _tokenHelper = tokenHelper;
        }

        // GET api/authentication/token
        [Authorize]
        [HttpGet]
        [Route("token")]
        public async Task<IActionResult> Token()
        {
            if(HttpContext.User == null || HttpContext.User.Identity == null) {
                return BadRequest("The user could not be authenticated.");
            }
            
            var accessToken = await _tokenHelper.CreateToken(HttpContext);
                if(accessToken != null && accessToken.Trim() != "")
                {
                    TokenProviderOptions _options = TokenHelper.GetTokenProviderOptions();
                    var token = new
                    {
                        access_token = accessToken,
                        expires_in = (int)_options.Expiration.TotalSeconds
                    };
                    return new OkObjectResult(token);
                }
                else {
                    return Unauthorized();
                }
            
        }


    }
}
