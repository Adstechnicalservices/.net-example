﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Net.Http.Server;

namespace Gateway
{
    public class App
    {
        public static void Main(string[] args)
        {
            
            var host = new WebHostBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseStartup<AppStartup>()
                .UseWebListener(options =>
                {
                    options.ListenerSettings.Authentication.AllowAnonymous = true;
                    options.ListenerSettings.Authentication.Schemes = AuthenticationSchemes.NTLM;
                
                })
                .Build();

            host.Run();
        }
    }
}
