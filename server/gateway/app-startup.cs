﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using Shared.Data.Nonpersisted;
using Shared.Helpers.Microservices;
using Shared.Utility.Authentication;
using Shared.Utility;

namespace Gateway
{
    public class AppStartup
    {
        public AppStartup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile("config/microservice-configuration.json", optional: false, reloadOnChange:true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }
        public static ILogger AppLogger;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            // Add functionality to inject IOptions<T>
            services.AddOptions();
             // Add our Microservices Configuration object so it can be injected
            services.Configure<MicroserviceConfiguration>(Configuration);
            services.AddScoped<HttpHelper>();
            services.AddScoped<UserHelper>();
            services.AddScoped<TokenHelper>();
            // Add framework services.
            services.AddAuthentication();
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            loggerFactory.AddFile("Logs/Gateway-{Date}.txt");
           AppStartup.AppLogger = loggerFactory.CreateLogger("Gateway");
           app.UseCors(builder =>
       builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin().AllowCredentials());
            MicroserviceConfiguration microservicesConfiguration = Configuration.GetSection("Microservices").Get<MicroserviceConfiguration>();

            //First do auth
            app.Use(async (context, next) => {
               TokenHelper _tokenHelper = context.RequestServices.GetService<TokenHelper>();
                if( !context.Request.Headers.ContainsKey("X-Delta-Authorization")) {
                    if(context.Request.Path == "/api/authentication/token") {              
                        await next.Invoke();
                    }
                    else {
                            await context.Response.WriteAsync("You must get a token at /api/authentication/token and pass "
                            + "it in as X-Delta-Authorization header before making subsequent calls.");
                    }
                }
                else {
                    AppStartup.AppLogger.LogInformation("About to verify token.");
                    string userId = await _tokenHelper.VerifyToken(context.Request.Headers["X-Delta-Authorization"]);
                    AppStartup.AppLogger.LogInformation("User Id after verification is " + userId);
                    if(userId != null && userId.Trim() != "") {                        
                        context.Request.Headers.Add("x-user-id", userId);
                        context.Items.Add("x-user-id", userId);
                        await next.Invoke();
                    } else {
                        if(!context.Response.HasStarted) {
                            context.Response.StatusCode = 401;
                            await context.Response.WriteAsync("There was an error validating the token.");
                        }                
                    } 
                }
            });

            //If going to api/authentication/token allow them to go to the controller
            app.MapWhen(context=> context.Request.Path.HasValue && context.Request.Path.Value.ToLower() == "/api/authentication/token", builder=> {
                builder.UseMvc();
            });

            //If they are authenticated, then proxy request to microservice
            if(microservicesConfiguration != null) {
                
                if(microservicesConfiguration.ActionItems != null && microservicesConfiguration.ActionItems.Details != null) { 
                    app.MapWhen(context=> context.Request.Path.HasValue && context.Request.Path.Value.ToLower() == "/api/actionitems", builder => builder.RunProxy(new ProxyOptions {
                        Scheme = microservicesConfiguration.ActionItems.Details.Scheme,
                        Host = microservicesConfiguration.ActionItems.Details.Host,
                        Port = microservicesConfiguration.ActionItems.Details.Port            
                    }));
                }

                if(microservicesConfiguration.Authorization != null && microservicesConfiguration.Authorization.Details != null) { 
                    app.MapWhen(context=> context.Request.Path.HasValue && context.Request.Path.Value.ToLower() == "/api/authorization", builder => builder.RunProxy(new ProxyOptions {
                        Scheme = microservicesConfiguration.Authorization.Details.Scheme,
                        Host = microservicesConfiguration.Authorization.Details.Host,
                        Port = microservicesConfiguration.Authorization.Details.Port            
                    }));
                }

                if(microservicesConfiguration.D3Integration != null && microservicesConfiguration.D3Integration.Details != null) { 
                    app.MapWhen(context=> context.Request.Path.HasValue && context.Request.Path.Value.ToLower().StartsWith("/api/d3"), builder=> builder.RunProxy(new ProxyOptions {
                        Scheme = microservicesConfiguration.D3Integration.Details.Scheme,
                        Host = microservicesConfiguration.D3Integration.Details.Host,
                        Port = microservicesConfiguration.D3Integration.Details.Port            
                    }));
                }

                if(microservicesConfiguration.Sources != null && microservicesConfiguration.Sources.Details != null) { 
                    app.MapWhen(context=> context.Request.Path.HasValue && context.Request.Path.Value.ToLower().Contains("documents"), builder=> builder.RunProxy(new ProxyOptions {
                        Scheme = microservicesConfiguration.Documents.Details.Scheme,
                        Host = microservicesConfiguration.Documents.Details.Host,
                        Port = microservicesConfiguration.Documents.Details.Port           
                    }));
                }

                app.MapWhen(context=> context.Request.Path.HasValue && context.Request.Path.Value.ToLower().Contains("forms"), builder=> builder.RunProxy(new ProxyOptions {
                    Scheme = microservicesConfiguration.Forms.Details.Scheme,
                    Host = microservicesConfiguration.Forms.Details.Host,
                    Port = microservicesConfiguration.Forms.Details.Port            
                }));

                app.MapWhen(context=> context.Request.Path.HasValue && context.Request.Path.Value.ToLower().Contains("notifications"), builder=> builder.RunProxy(new ProxyOptions {
                    Scheme = microservicesConfiguration.Notifications.Details.Scheme,
                    Host = microservicesConfiguration.Notifications.Details.Host,
                    Port = microservicesConfiguration.Notifications.Details.Port           
                }));

                if(microservicesConfiguration.Sources != null && microservicesConfiguration.Sources.Details != null) { 
                    app.MapWhen(context=> context.Request.Path.HasValue && context.Request.Path.Value.ToLower().Contains("sources"), builder=> builder.RunProxy(new ProxyOptions {
                        Scheme = microservicesConfiguration.Sources.Details.Scheme,
                        Host = microservicesConfiguration.Sources.Details.Host,
                        Port = microservicesConfiguration.Sources.Details.Port            
                    }));
                }

            }
            //Catch all response
            app.Run(async context=> {
                 context.Response.StatusCode = 500;
                await context.Response.WriteAsync("There was an error routing your request.");
            });
        }
    }
}
