using System;

namespace Shared.Data.Persisted {
    public class User {
                public Guid Id {get;set;}
        public Guid Delta3Id {get;set;}
        public string Title {get;set;}
        public string FirstName {get;set;}
        public string MiddleName {get;set;}
        public string LastName {get;set;}
        public string OtherName {get;set;}
        public string Username {get;set;}
        public string EmployeeId {get;set;}
        public string WorkAreaUrl {get;set;}
        public string Agency {get;set;}
        public string Email {get;set;}
        public bool IsActive {get;set;}
        public string Notes {get;set;}
        public User() {

        }
    }
}