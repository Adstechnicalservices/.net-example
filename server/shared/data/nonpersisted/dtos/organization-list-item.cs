
using System;
namespace Shared.Data.Nonpersisted.Dtos {
    public class OrgUserDto
    {
        public Guid UserId { get; set; }
        public string LoginName { get; set; }
        public string WorkAreaUrl { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string OtherName { get; set; }
        public string Title { get; set; }
        public string Agency { get; set; }
        public string Email { get; set; }
        public string EmployeeId { get; set; }
        public string Notes { get; set; }
        public bool? IsActive { get; set; }
    }
}