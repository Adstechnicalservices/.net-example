
namespace Shared.Data.Nonpersisted.Dtos {
    public class UserDto
    {

        public string _Id { get; set; }
        public bool IsInternal { get; set; }
        public string Display { get; set; }
        public string AltDisplay { get; set; }
    }
}