using System;
using System.Collections.Generic;

namespace Shared.Data.Nonpersisted.Dtos {
    public class FormDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string FormType { get; set; }
        public string SourceNumber { get; set; }
        public Guid SourceId { get; set; }
        public string Status { get; set; }
        public string AssignedTo { get; set; }
        public DateTimeOffset? LastModified { get; set; }
        public DateTimeOffset? SerializedDate { get; set; }
        public string CreatedBy { get; set; }
        public string Synopsis { get; set; }
        public string AssociatedDocument { get; set; }
        public int AssociatedDocumentsNumber { get; set; }
        public List<string> Attachments { get; set; }

        public FormDto()
        {
            Attachments = new List<string>();
        }
    }
}