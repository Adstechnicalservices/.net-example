using System;

namespace Shared.Data.Nonpersisted.Dtos {
public class ActionItemDto {
    public ActionItemDto() {
        
    }
    public string _id {get;set;}
    public Guid LegacyId {get;set;}
    public string Action {get;set;}
    public string SourceNumber {get;set;}
    public DateTimeOffset Date {get;set;}
    public string Squad {get;set;}

}

}