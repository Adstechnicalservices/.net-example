using System;
using System.Collections.Generic;
using Shared.Data.Nonpersisted.Dtos;


namespace Shared.Data.Nonpersisted.Dtos {
    public class NotificationDto
    {
        public string _Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string SourceNumber { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public List<NotificationAudienceDto> Audience { get; set; }
        public string Status { get; set; }
        public NotificationDto()
        {
            Audience = new List<NotificationAudienceDto>();
            CreatedOn = DateTimeOffset.Now;
        }
    }
public class NotificationAudienceDto
{
    public UserDto User { get; set; }
    public bool active { get; set; }
        public NotificationAudienceDto()
        {
            User = new UserDto();
        }
}
}