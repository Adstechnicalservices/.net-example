using System;
namespace Shared.Data.Nonpersisted.Dtos {
    public class SerializedFormListItemDto
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string FileName { get; set; }
        public string FolderName { get; set; }
        public Guid? FormTypeId { get; set; }
        public Guid? SourceId { get; set; }
        public DateTimeOffset? SerializedDate { get; set; }
        public DateTimeOffset? LastModified { get; set; }
        public string Author { get; set; }
        public string Synopsis { get; set; }
        public string ContentType { get; set; }
        public string AssociatedDocument { get; set; }
        public string DocumentType { get; set; }
        public string Url { get; set; }
        public bool CanShowReview { get; set; }
    }
}