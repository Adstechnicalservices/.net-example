
using System;

namespace Shared.Data.Nonpersisted.Dtos {
    public class SourceListItemDto
    {
        public string _Id {get;set;}
        public Guid Id { get; set; }
        public string SourceNumber { get; set; }
        public string PaymentName { get; set; }
        public string Status { get; set; }
        //public string OwningFieldOffice { get; set; }
        public string CaseAgent { get; set; }
        //public string AgentFieldOffice { get; set; }
        //public string AgentSquad { get; set; }
        public DateTime? LastOpeningDate { get; set; }
        public DateTime? LastQssrDate { get; set; }
        public DateTime? LastFoasrDate { get; set; }
        public string OiaExpiresDate { get; set; }
        public string MyRole { get; set; }
        //public string[] CoCaseAgents { get; set; }
    }
}