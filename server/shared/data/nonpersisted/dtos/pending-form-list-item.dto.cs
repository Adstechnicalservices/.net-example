
using System;
namespace Shared.Data.Nonpersisted.Dtos {
    public class PendingFormListItemDto
    {
        public Guid Id { get; set; }
        public string FormType {get;set;}
        public string SourceNumber {get;set;}
        public string Status {get;set;}
        public string AssignedTo {get;set;}
        public DateTimeOffset? LastModified {get;set;}
        public string CreatedBy {get;set;}
        public string Synopsis {get;set;}
    }
}