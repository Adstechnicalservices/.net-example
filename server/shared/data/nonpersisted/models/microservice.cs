

namespace Shared.Data.Nonpersisted {
    public class Microservice {
       public string FriendlyName {get;set;}
       public string Description {get;set;}
       public MicroserviceHostingDetails Details {get;set;}
    }

    public class MicroserviceHostingDetails {
        public string Scheme {get;set;} = "http";
        public string Host {get;set;} = "localhost";
        public string Port {get;set;}
    }
}