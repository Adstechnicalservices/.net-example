using System;
using System.Collections.Generic;
namespace Shared.Data.Nonpersisted {

    public class MicroserviceConfiguration {
        public Microservice ActionItems {get;set;}
        public Microservice Authorization {get;set;}
        public Microservice D3Integration {get;set;}
        public Microservice Documents {get;set;}
        public Microservice Forms {get;set;}
        public Microservice Notifications {get;set;}
        public Microservice Sources {get;set;}
    }
}