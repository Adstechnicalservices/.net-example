

namespace Shared.Data.Nonpersisted.Models {
    public class Endpoint {
        public string Http {get;set;}
        public string Https {get;set;}
    }
}