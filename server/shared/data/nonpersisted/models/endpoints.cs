

namespace Shared.Data.Nonpersisted.Models {

    public class Endpoints {
        public Endpoint Dev {get;set;}
        public Endpoint Int {get;set;}
        public Endpoint Test {get;set;}
        public Endpoint QA {get;set;}
        public Endpoint Prod {get;set;}
    }
}