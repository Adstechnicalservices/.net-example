
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Shared.Interfaces.RestfulService {
public interface IRestServiceLogic<PKType, Type> {
    Task<Type> FindOne(PKType id);
    Task<List<Type>> Find();
    Task<Type> Create(Type newRecord);
    Task<Type> Update(PKType id, Type updatedRecord);
    Task<bool> Delete(PKType id);
}


}