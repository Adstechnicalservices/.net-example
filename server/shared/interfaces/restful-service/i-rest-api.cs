
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace Shared.Interfaces.RestfulService {
public interface IRestServiceApi<PKType, Type> {
    Task<IActionResult> FindOne(PKType id);
    Task<IActionResult> Find();
    Task<IActionResult> Create(Type newRecord);
    Task<IActionResult> Update(PKType id, Type updatedRecord);
    Task<IActionResult> Delete(PKType id);
}

}