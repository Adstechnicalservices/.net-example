
using System;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Shared.Data.Persisted;
using Shared.Utility;

namespace Shared.Helpers.Microservices {
    public class UserHelper {
        private HttpHelper _httpHelper;
        public UserHelper(HttpHelper httpHelper) {
            _httpHelper = httpHelper;
        }
        public async Task<Guid> GetUserId(string username) {
                Guid userId = Guid.Empty;
                var httpHandler = new HttpClientHandler() {
                UseDefaultCredentials = true
            };
            
            string userIdString = await _httpHelper.GetJson(httpHandler, "http://localhost:5010", "/api/users/getDelta3UserId/" + username);
            
            Guid.TryParse(userIdString.Split('"')[1], out userId);
            return userId;
        }

        public async Task<User> GetUser(string username) {
            User user = null;

            return user;
        }

        public async Task<User> GetUser(Guid userId) {
            User user = null;

            return user;
        }
    }
}