
using System;
using Microsoft.AspNetCore.Http;
using System.Net.Http;
using System.Threading.Tasks;
using RestSharp;
using RestSharp.Authenticators;

namespace Shared.Helpers {
    public class RequestProxyer {

        static RestClient client = new RestClient();
        
        public static async Task<RestResponse> ProxyRequest(HttpContext context) {   
            client.BaseUrl= GetBaseUrl();
            AddDefaultHeaders(context.Request.Headers);
            var request = BuildRequest(context);
            var response = await client.ExecuteAsync(request);
            return response;
        }

        private static RestRequest BuildRequest(HttpContext context) {

            var request = new RestRequest();
            request.Resource = context.Request.Path;

            switch (context.Request.Method) {
                case "POST" : {
                    request.Method = RestSharp.Method.POST;
                    request.AddJsonBody(context.Request.Body);
                  break;
                }
                case "PUT": {
                    request.Method = RestSharp.Method.PUT;
                    request.AddBody(context.Request.Body);
                    break;
                }
                case "GET": {
                    request.Method = RestSharp.Method.GET;
                    break;
                }
                case "DELETE": {
                    request.Method = RestSharp.Method.DELETE;
                    break;
                }
                case "PATCH" : {
                    request.Method = RestSharp.Method.PATCH;
                    break;
                }
                case "OPTIONS" : {
                    request.Method = RestSharp.Method.OPTIONS;
                    break;
                }
                  default:
                  break;
            }

            return request;
        }

        private static Uri GetBaseUrl() {
           return new Uri("http://localhost:6007");
        }

        private static void AddDefaultHeaders(IHeaderDictionary headers) {
            foreach(var header in headers) {
                if(header.Value.Count > 1) {
                    foreach(var val in header.Value.ToArray()) {
                        client.AddDefaultHeader(header.Key, val);
                    }
                }
                else {
                    client.AddDefaultHeader(header.Key, header.Value.ToString());
                }
            }
        }
    }
}