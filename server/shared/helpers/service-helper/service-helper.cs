
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Shared.Helpers {

interface IServiceHelper {
   // Task<string> GetServiceEndpoint();
    Task<RestSharp.RestResponse> ProxyRequest(HttpContext context);
}

    public class ServiceHelper : IServiceHelper {

        public async Task<RestSharp.RestResponse> ProxyRequest(HttpContext context) {
            return await RequestProxyer.ProxyRequest(context);
        }

    }
}