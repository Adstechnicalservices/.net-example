
using System;
using Microsoft.AspNetCore.Http;
using System.Net.Http;
using System.Threading.Tasks;
using RestSharp;
namespace Shared.Helpers {
    public static class RestSharpExtension
{
    public static async Task<RestResponse> ExecuteAsync(this RestClient client, RestRequest request)
    {
        TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
        RestRequestAsyncHandle handle = client.ExecuteAsync(request, r => taskCompletion.SetResult(r));
        return (RestResponse)(await taskCompletion.Task);
    }
}
}