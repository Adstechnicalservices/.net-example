using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;

namespace Shared.Utility.Authentication
{

    public abstract class TokenConfig {
            public static readonly string secretKey = "DeltaRocks1!!P@ssw0rd";
        public static SymmetricSecurityKey signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));
        public static TokenProviderOptions options = new TokenProviderOptions
        {
            Audience = "DeltaUsers",
            Issuer = "DeltaApplication",
            SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256),
        };
        public static TokenValidationParameters tokenValidationParameters = new TokenValidationParameters
        {
            // The signing key must match!
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = signingKey,

            // Validate the JWT Issuer (iss) claim
            ValidateIssuer = true,
            ValidIssuer = options.Issuer,

            // Validate the JWT Audience (aud) claim
            ValidateAudience = true,
            ValidAudience = options.Audience,

            // Validate the token expiry
            ValidateLifetime = true,

            // If you want to allow a certain amount of clock drift, set that here:
            ClockSkew = TimeSpan.Zero
        };
    }
}