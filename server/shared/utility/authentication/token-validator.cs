using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;

namespace Shared.Utility.Authentication
{

    public class TokenValidator {

public static ClaimsPrincipal DecryptToken(string authHeader) {
    var jwtString = ExtractJwt(authHeader);
    if(jwtString == null || jwtString.Trim() == "") {
        return null;
    }
    var principal = GetDecryptedToken(jwtString);
    if(principal == null) {
        return null;
    }
    return principal;
}

private static string ExtractJwt(string authHeader)
        {
            string jwt = "";
            if (authHeader != null && authHeader.Trim() != "")
            {
               // _logger.LogInformation("In ExtractJwt.  Going to extract from {authHeader}", authHeader);
                jwt = authHeader.Split(' ')[1];
            }
            return jwt;
        }

        private static ClaimsPrincipal GetDecryptedToken(string jwtString)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken token;
          var principal = tokenHandler.ValidateToken(jwtString, TokenHelper.GetTokenValidationParameters(), out token);

            return principal;
        }
    }

}