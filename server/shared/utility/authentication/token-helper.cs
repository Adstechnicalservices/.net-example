using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Shared.Helpers.Microservices;
using System.Linq;

namespace Shared.Utility.Authentication
{
    public class TokenHelper
    {
        private UserHelper _userHelper;
        public TokenHelper(UserHelper userHelper) {
            _userHelper = userHelper;
        }

        public static TokenProviderOptions GetTokenProviderOptions()
        {
            return TokenConfig.options;
        }

        public static TokenValidationParameters GetTokenValidationParameters()
        {
            return TokenConfig.tokenValidationParameters;
        }

        public async Task<string> CreateToken(HttpContext context)
        {
            var username = context.User.Identity.Name.Split('\\')[1];
            if(username != null && username.Trim() != "") {
               var userId = await _userHelper.GetUserId(username);
               var token = await TokenGenerator.GenerateToken(userId, username);
               return token;
            }
            else {
                return null;
            }
        }

        public async Task<string> VerifyToken(string authHeader) {
            try {

                var principal = TokenValidator.DecryptToken(authHeader);

                var uidClaim = principal.Claims.Where(c => c.Type == "uid").FirstOrDefault();
                var userId = uidClaim.Value;
                return userId;

            }
            catch(Exception ex) {
                return null;
            }
        }

       
    }
}