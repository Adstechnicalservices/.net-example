using Microsoft.Extensions.Logging;

namespace Shared.Helpers {

public static class AppLoggerFactory
{
  public static ILoggerFactory LoggerFactory {get;} = new LoggerFactory();
  public static ILogger CreateLogger<T>() =>
    LoggerFactory.CreateLogger<T>();
}

}