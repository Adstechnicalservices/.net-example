

///////////////

app-logger.cs --- This is a static implementation of the .NET Core logging as demonstrated by this article : https://msdn.microsoft.com/en-us/magazine/mt694089.aspx

To be used as such:
ILogger Logger { get; } =
    ApplicationLogging.CreateLogger<Controller>();
Logger.LogInformation("Logger created");

//////////////