using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using System.Net.Http;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace Shared.Utility {
    public class HttpHelper {
        ILoggerFactory loggerFactory;
        ILogger logger;
        public HttpHelper() {
            loggerFactory = new LoggerFactory().AddDebug().AddFile("Logs/HttpHelper-{Date}.txt");
            logger = loggerFactory.CreateLogger("HttpHelper");
        }
        public async Task<List<T>> GetMultiple<T>(HttpClientHandler handler, string baseAddress, string requestPath) {
            logger.LogInformation("Ok in Get Multiple");
            logger.LogInformation("Handler  is not null? " + (handler != null));
            logger.LogInformation("The base address is? " + baseAddress);
            logger.LogInformation("The request path is? " + requestPath);
            var httpClient = (handler != null) ? new HttpClient(handler) : new HttpClient();
            httpClient.BaseAddress = new Uri(baseAddress);
            HttpResponseMessage response = await httpClient.GetAsync(requestPath);
            logger.LogInformation("After await is success status? " + response.StatusCode);
            if(response.IsSuccessStatusCode) {
                var json = await response.Content.ReadAsStringAsync();
                logger.LogInformation("The json back is " + json);
                var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<T>>(json);
                logger.LogInformation("List count is " + list.Count());
                return list;
            }
            else {
                return null;
            }
        }

        public async Task<T> GetSingle<T>(HttpClientHandler handler, string baseAddress, string requestPath) {
            var httpClient = (handler != null) ? new HttpClient(handler) : new HttpClient();
            httpClient.BaseAddress = new Uri(baseAddress);
            HttpResponseMessage response = await httpClient.GetAsync(requestPath);
            if(response.IsSuccessStatusCode) {
                var json = await response.Content.ReadAsStringAsync();
                T record = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);
                return record;
            }
            else {
                return default(T);
            }
        }

        public async Task<string> GetJson(HttpClientHandler handler, string baseAddress, string requestPath) {
            var httpClient = (handler != null) ? new HttpClient(handler) : new HttpClient();
            httpClient.BaseAddress = new Uri(baseAddress);
            HttpResponseMessage response = await httpClient.GetAsync(requestPath);
            if(response.IsSuccessStatusCode) {
                var json = await response.Content.ReadAsStringAsync();
                return json;
            }
            else {
                return null;
            }
        }
    }
}