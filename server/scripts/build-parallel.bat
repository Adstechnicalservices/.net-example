
start cmd.exe /k "cd ../gateway && dotnet build"
start cmd.exe /k "cd ../microservices/action-items && dotnet build"
start cmd.exe /k "cd ../microservices/authentication && dotnet build"
start cmd.exe /k "cd ../microservices/d3-integration && dotnet build"
start cmd.exe /k "cd ../microservices/notifications && dotnet build"
start cmd.exe /k "cd ../microservices/forms && dotnet build"
start cmd.exe /k "cd ../microservices/documents && dotnet build"