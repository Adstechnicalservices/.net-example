
start cmd.exe /quiet "cd ../gateway && SET ASPNETCORE_URLS= http://localhost:5000 && dotnet run"
start cmd.exe /quiet "cd ../microservices/action-items && SET ASPNETCORE_URLS= http://localhost:5001 && dotnet run"
start cmd.exe /quiet "cd ../microservices/authentication && SET ASPNETCORE_URLS= http://localhost:5002 && dotnet run"
start cmd.exe /quiet "cd ../microservices/d3-integration && SET ASPNETCORE_URLS= http://localhost:5003 && dotnet run"