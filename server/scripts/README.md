Simple batch files or Powershell files

/////
    build-parallel.bat -- (deprecated) Runs build in parallel - however, may need to be deleted as a common DLL dependency with the shared.dll can cause issues
///////

//////
    build-sequential.bat -- Runs build for every service using the dotnet CLI, one after another.  The same build that is performed when you click on the Run All in Debug tab essentially.
//////////

/////////
    install-dependencies.bat -- Run this when you first clone project to run a dotnet restore for all projects.
////////

///////
    kill-all-windows.bat -- Helper batch file for quickly killing command windows
////////

/////////
    publish.bat -- Essentially performs the dotnet publish command that is performed on the build server, to verify publish locally
////////////

///////
    run-debug.bat -- (deprecated)  Preferred debug method is to use the IDE so the services run AND you get breakpoint support.
/////////