import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers }  from '@angular/http';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class AppService {
  options: RequestOptions;
    constructor(private http: Http) {
        console.log('Instantiating App Service');

        this.options = new RequestOptions({
            withCredentials: true
        });
    }

        public getToken(): Promise<any> {
            return this.http.get('http://localhost:5000/api/authentication/token', this.options).toPromise().then((result)=> {
                console.log("Success");
                localStorage.setItem('access_token', result.json().access_token);
                return result.json();
            }, (error)=> {
                console.log("There was error " + error);
            }).catch((error)=> {
                console.log("Hit catch " + JSON.stringify(error));
            });
        }
    }

