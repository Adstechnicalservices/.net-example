import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers }  from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class HomeService {
  options: RequestOptions;
    constructor(private http: Http) {
        console.log('Instantiating Home Service');
            let headers = new Headers({
            'Accept': 'application/json',
            'X-Authorization': 'Bearer ' + localStorage.getItem('access_token')
        });
        this.options = new RequestOptions({
        headers: headers
        });
    }

    public getActionItems(): Promise<any> {
        let d3ActionItems = this.http.get('http://localhost:5000/api/d3actionitems', this.options).toPromise().then((result)=> {
            console.log("Success");
           return result.json();
       }, (error)=> {
           console.log("There was error " + error);
       }).catch((error)=> {
           console.log("Hit catch " + JSON.stringify(error));
       });
       let actionItems = this.http.get('http://localhost:5000/api/actionitems', this.options).toPromise().then((result)=> {
            console.log("Success");
            return result.json();
        }, (error)=> {
            console.log("There was error " + error);
        }).catch((error)=> {
            console.log("Hit catch " + JSON.stringify(error));
        });
       return d3ActionItems && actionItems;
    }

    public getPendingForms() {
         return this.http.get('http://localhost:5000/api/d3pendingforms', this.options).toPromise().then((result)=> {
            console.log("Success");
           return result.json();
       }, (error)=> {
           console.log("There was error " + error);
       }).catch((error)=> {
           console.log("Hit catch " + JSON.stringify(error));
       });
    }

}
