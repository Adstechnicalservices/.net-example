import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpModule} from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';
import { HomeActionItemsGridComponent} from './action-items/action-items.component';
import {HomePendingFormsGridComponent} from './pending-forms/pending-forms.component';
export const routes = [
  { path: '', component: HomePage, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    HomePage,
    HomeActionItemsGridComponent,
    HomePendingFormsGridComponent
  ]
})

export class HomeModule { }
