import { Component, ViewEncapsulation } from '@angular/core';
import {HomeService} from './home.service';

@Component({
  selector: 'app-home-page',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './home.page.html',
  providers: [ HomeService ]
})
export class HomePage  {
  public result: any = "loading...";

  private _homeService: HomeService;
    constructor(public homeService: HomeService) {
      console.log("Initializing home page");
      this._homeService = homeService;
    }

    ngOnInit() {
      console.log('Initializing the home page.');
      this.loadPageData();
    }

    private loadPageData() {
      this._homeService.getActionItems().then((result) => {
        console.log(result);
       this.result = result;
      }).catch((err) => {
        console.log("Hit the catch of loadPageData.  Error is " + JSON.stringify(err));
      });

      this._homeService.getPendingForms();

    }
}
