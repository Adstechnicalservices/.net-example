import { Component, ViewEncapsulation } from '@angular/core';
import {PendingForm} from './pending-form.model';
import {HomeService} from '../home.service';

@Component({
  selector: 'home-pending-forms-grid',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './pending-forms.component.html',
  providers: [ HomeService ]
})
export class HomePendingFormsGridComponent  {
  public pendingForms: Array<PendingForm>;

    constructor(public homeService: HomeService) {
      console.log("Initializing home component HomePendingFormsGridComponent");
    }

    ngOnInit() {
      console.log('Initializing the HomePendingFormsGridComponent.');
      this.loadData();
    }

    private loadData() {
      this.homeService.getPendingForms().then((pendingForms : Array<PendingForm>) => {
        console.log(pendingForms);
       this.pendingForms = pendingForms;
      }).catch((err) => {
        console.log("Hit the catch of loadData in SourceListItemsGridComponent.  Error is " + JSON.stringify(err));
      });
    }
}
