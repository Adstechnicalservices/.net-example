
export class PendingForm {
    id: string;
    formType:string;
    sourceNumber: string;
    status: string;
    assignedTo: string;
    lastModified: Date;
    createdBy: string;
    synopsis: string;
}
