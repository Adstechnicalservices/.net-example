
export class ActionItem {
    public _id : string;
    public legacyId : string;
    public action : string;
    public sourceNumber : string;
    public date: Date;
    public squad : string;
}