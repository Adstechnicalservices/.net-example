import { Component, ViewEncapsulation } from '@angular/core';
import {ActionItem} from './action-item.model';
import {HomeService} from '../home.service';

@Component({
  selector: 'home-action-items-grid',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './action-items.component.html',
  providers: [ HomeService ]
})
export class HomeActionItemsGridComponent  {
  public actionItems: Array<ActionItem>;

    constructor(public homeService: HomeService) {
      console.log("Initializing home component ActionItemsGrid");
    }

    ngOnInit() {
      console.log('Initializing the ActionItemsGrid.');
      this.loadData();
    }

    private loadData() {
      this.homeService.getActionItems().then((actionItems) => {
        console.log(actionItems);
       this.actionItems = actionItems;
      }).catch((err) => {
        console.log("Hit the catch of loadData.  Error is " + JSON.stringify(err));
      });
    }
}
