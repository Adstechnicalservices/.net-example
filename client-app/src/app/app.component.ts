import { Component, ViewEncapsulation } from '@angular/core';
import { AppService } from './app.service';
@Component({
  selector: 'app-root',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss', './theme/components/navbar/navbar.component.scss']
})

export class AppComponent {
  public isLoading: boolean = true;
  public isAuthenticated : boolean = false;
  public acceptedTermsOfUse : boolean = false;

  constructor(public appService : AppService) {
    console.log("Constructing the app...");
  }

  ngOnInit() {
    console.log("In ngOnInit");
    this.intialize();
  }

  
  public onUserAgreed() {
    sessionStorage.setItem('user_agreed', 'yes');
    this.acceptedTermsOfUse = true;
  }

  private async intialize() {
    console.log("In initialize.");
    if(await this.verifyIsDeltaUser()) {
      console.log("User is verified.  User Agreed is " + sessionStorage.getItem('user_agreed'));
      //Refreshed page but already had agreed
      if(sessionStorage.getItem('user_agreed') == 'yes') {
        this.acceptedTermsOfUse = true;
      }
    }

  }

  // ngAfterViewInit() {
  //   setTimeout(()=> {
  //     this.isLoading = false;
  //     this.isAuthenticated = true;
  //   }, 3000);
  // }

  private verifyIsDeltaUser() : Promise<boolean> {
   return this.appService.getToken().then((result) => {
      this.isLoading = false;
            if(result && result.access_token) {
                this.isAuthenticated = true;
                return true;
            } else { 
              this.isAuthenticated = false;
              return false;
            }
        }).catch((error)=> {
          this.isAuthenticated = false;
          return false;
        });
  }


 }
